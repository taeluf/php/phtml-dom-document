<?php

namespace Tlf\Phtml\Test;

class TextNodes extends \Tlf\Tester {

    public function testIsTagName(){

        $view = <<<HTML
            <div item="Blog" deleteable>
                text node
            </div>
        HTML;
        $doc = new \Taeluf\PHTML($view);

        $items = $doc->xpath('//*[@item]');
        $text = $items[0]->children[0];


        $this->is_true($text->nodeName == '#text');
        $this->is_true($text->is('#text'));
        $this->is_false($text->is(''));
        $this->is_false($text->is('div'));


    }
}
