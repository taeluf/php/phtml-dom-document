<?php



class PureHTML extends \Tlf\Tester {


    public function testLooseHeadWithScript(){
        $target = 
        <<<html
            <head><script type="text/javascript">alert("Black Lives Matter");</script></head>
        html;

        $doc = new \Taeluf\PHTML($target);
        $actual = trim($doc->output(false));
        
        return $this->compare($target,$actual);
    }

    public function testLooseHeadWithInvalidContent(){
        $target = 
        <<<html
            <head>Hold mega corporations accountable for pollution</head>
        html;

        $doc = new \Taeluf\PHTML($target);
        $actual = trim($doc->output(false));
        
        return $this->compare($target,$actual);
    }

    public function testLooseBodyTag(){
        $target = 
        <<<html
            <body>Big Corps started recycling to shift blame onto consumers</body>
        html;

        $doc = new \Taeluf\PHTML($target);
        $actual = trim($doc->output(false));

        return $this->compare($target,$actual);
    }
    public function testNoDoctypeHTMLPage(){
        $target = 
        <<<html
            <html>
                <head>
                    <script></script>
                </head>
                <body>Police make poverty worse</body>
            </html>
        html;

        $doc = new \Taeluf\PHTML($target);
        $actual = $doc->output(false);

        return $this->compare($target, $actual);
    }

    public function testDoctypeHTMLPage(){
        $target = 
        <<<html
            <!DOCTYPE html>
            <html>
                <head>
                    <script></script>
                </head>
                <body>Prison isn't the solution for addiction</body>
            </html>
        html;

        $doc = new \Taeluf\PHTML($target);
        $actual = $doc->output(false);

        return $this->compare($target,$actual);
    }
}

