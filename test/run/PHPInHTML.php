<?php



class PHPInHTML extends \Tlf\Tester {

    public function testPHPInHead(){
        $target = 
        <<<PHP
            <!DOCTYPE html>
            <html>
                <head>
                    <script></script>
                    <?php echo "I am some php code"; ?>
                </head>
                <body>Prison isn't the solution for addiction</body>
            </html>
        PHP;

        $doc = new \Taeluf\PHTML($target);
        $actual = $doc->output();

        $this->compare($target,$actual);
    }
    public function testPHPAsAttributeValue(){
        $php = 
        <<<PHP
            <div data-happy="<?='Love eachother'?>">Kindness is awesome</div>
        PHP;
        $target = 
        <<<HTML
            <div data-happy="Love eachother">Kindness is awesome</div>
        HTML;

        $doc = new \Taeluf\PHTML($php);

        file_put_contents($outFile=__DIR__.'/../output/PHPAsAttributeValue.phtml',$doc);
        ob_start(); 
        require($outFile); 
        $output = trim(ob_get_clean());

        $this->compare($target, $output);
    }
    public function testPHPAsAttribute(){
        $php = 
        <<<PHP
            <div <?php echo "data-attr=\"good\""; ?> ><?php echo "text"; ?></div>
        PHP;

        $doc = new \Taeluf\PHTML($php);
        file_put_contents($outFile=__DIR__.'/../output/PHPAsAttribute.phtml',$doc);
        ob_start();
        require($outFile);
        $content = trim(ob_get_clean());

        $this->compare(
            "<div data-attr=\"good\">text</div>",
            $content
        );
    }
    public function testPHPAsNode(){
        $php = '<div><?php echo "Scripts won\'t save us from corruption." ?></div>';
        $doc = new \Taeluf\PHTML($php);
        $output = $doc."";
        file_put_contents($outFile=__DIR__.'/../output/PHPAsNode.phtml',$output);
        ob_start();
        require($outFile);
        $content = ob_get_clean();


        $this->compare(
            "<div>Scripts won't save us from corruption.</div>",
            $content
        );
    }
}

