<?php

namespace Tlf\Phtml\Test;

class Nodes extends \Tlf\Tester {


    public function testHideOwnTagProgrammatic(){

        $view = <<<HTML
            <div item="Blog">
                <h1>title</h1>
                <p>para</p>
            </div>
        HTML;
        $doc = new \Taeluf\PHTML($view);

        $node = $doc->firstChild->children[1];
        $node->hideOwnTag = true;
        
        $this->compare_lines(
            <<<HTML
                <h1>title</h1>
                <p>para</p>
            HTML,
            $node
        );
    }
    public function testHideOwnTagNode(){

        $view = <<<HTML
            <div item="Blog" hideOwnTag="true">
                <h1>title</h1>
                <p>para</p>
            </div>
        HTML;
        $doc = new \Taeluf\PHTML($view);

        $this->compare_lines(
            <<<HTML
                <h1>title</h1>
                <p>para</p>
            HTML,
            $doc->firstChild->children[1],
        );
    }

    public function testHideOwnTagDoc(){

        $view = <<<HTML
            <div item="Blog" hideOwnTag="true">
                <h1>title</h1>
                <p>para</p>
            </div>
        HTML;
        $doc = new \Taeluf\PHTML($view);

        $this->compare_lines(
            <<<HTML
                <h1>title</h1>
                <p>para</p>
            HTML,
            $doc
        );
    }

    public function testIsAttribute(){

        $view = <<<HTML
            <div item="Blog" deleteable>
                <h1 prop="title"></h1>
                <p prop="intro"></p>
            </div>
        HTML;
        $doc = new \Taeluf\PHTML($view);

        $items = $doc->xpath('//*[@item]');


        $this->compare('', $items[0]->deleteable);
        $this->is_true($items[0]->has('deleteable'));
        $this->is_true($items[0]->isDeleteable());

    }
}
