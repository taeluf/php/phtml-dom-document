<?php

namespace Taeluf\PHTML;

class DOMComment extends \DOMComment {


    /** is this node the given tag
    */
    public function is(string $tagName): bool{
        if (strtolower($this->nodeName)==strtolower($tagName))return true;
        return false;
    }

    public function __toString(){
        return $this->ownerDocument->saveHTML($this);
    }
}
