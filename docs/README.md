# pHtml, an easier, better DOMDocument
A DOMDocument extension that's easier to use, allows PHP code, and provides a few extra apis for working with the document.

## Status: Stable, tests passing
I don't expect any existing functionality to break. I'm not aware of any major bugs, but if you find one, open an issue & I'll try to fix it. I have no idea what happens if you use it on XML.

## Install
- `composer require taeluf/phtml 0.1.*` OR `{"require": {"taeluf/phtml": "0.1.*"}}`
    - Dev version is `v0.1.x-dev`.

### Document
- Construct with `$doc = new \Taeluf\PHTML($htmlAndPHPSource)`, instead of using `loadHtml()`
- Allows PHP anywhere in the document
    - Use `$doc->output(false);` to remove PHP code
- `echo $doc` will show your document. 
    - `$doc->output(true)` is called internally (`true` restores your PHP code from placeholders)
- Call `$doc->xpath('//tagname[@attributename="value"]', $refNode=null)` to query with xpath
    - Returns an array of nodes, not a node list
    - `$refNode` may be null to search from root of document or it may be a node within the document to search under
- It IS a `DOMDocument` so all the functions are available as [documented on the php site](https://www.php.net/manual/en/class.domdocument.php).
- `$doc->childNodes[0]` is the root node that's added by pHtml. 
    - The 1st node declared in your html is at `$doc->childNodes[0]->childNodes[0];`

### Node
- Call `$node->attributes()` to get all HTML attributes in an array
- `$node->doc` returns the DOMDocument instance
- `$node->innerHTML = "whatever"` and `$var = $node->innerHTML` both work
- `$inputNode->form` returns the parent form node, if it exists
- `$node->attribute` returns the named attribute
- `$node->attribute = "funny"` sets the named attribute to the value
- `unset($node->attribute)` removes the attribute
- `echo $node` echos the outer html
    - This will not restore PHP, though. Placeholders will still be present
    - call `$doc->fillWithPHP("".$node);` to get the node WITH php
- `$formNode->addHiddenInput($name,$value)` will... do that
- `$node->boolAttribute('attributeName');` - currently just calls `$node->hasAttriute($attrName)`, but I had plans to do something else. Those plans were not standars-compliant, though.

### Compiler
The compiler is NOT used by this library. I'm using it in another lib. I use this to modify PHP in an HTML document in a fine-tuned manner. I don't know if this will remain a part of `PHTML` or not. It uses the PHP parser, and provides some niceties around the PHP without using a DOMDocument
- Converts PHP + HTML code to HTML code with placeholders
- Output to a file
- Prepend code before or append code after a placeholder or before/after the full block of code

### Extra Stuff
- Call `$html = $doc->phpPlaceholder('<?php echo "A block of PHP code>?>');`
    - returns an alpha-numeric string if it's just PHP code, or modified HTML with the placeholder(s) present
    - the placeholder is stored in `PHTMLDoc` so you can add your placeheld code to the document & it will be restored when you output the document.
    - Later call `$doc->fillWithPHP($html)` to restore the PHP.
    - You can pass a larger block of HTML code as well, then use the `fillWithPHP` to restore it to it's former glory.
    - This is used internally

## Weird Stuff / Warnings
- `<!DOCTYPE>`, `<html>`, and `<head>` tags are converted to `<tlfphtml-doctype>`, `<tlfphtml-html>`, and `<tlfphtml-head>` before the DOMDocument is created. They are converted back to the proper DOCTYPE!/html/head tag during the `__toString()`

### xpath stuff
- Get all children of node: `$phtml->xpath('descendant::tagname[@attrName="attrval"]', $contextNode)` <- the `descendant::` part need stay the same. The rest of the query is up to you.
- to `xpath` for doctype, html, or head, use `//tlfphtml-doctype`, `//tlfphtml-html`, and `//tlfphtml-head` respectively.

## TODO
- Install instructions
- Notes for contributors
- Documentation website & Better documentation

## Implementation Details
- PHP is compiled out and replaced with alpha-numeric placeholders before being added to PHP's built-in DOMDocument. Then AFTER the document is output, the PHP placeholders are replaced with the original PHP code
- Wraps all code in `<root></root>`, but does not output that. I think this was necessary for making the `__toString()` work correctly...

## Bugs
- breaks if an open PHP tag (`<?php`) is not later closed (`?>`)
