# File code/PHPParser.php  
  
# class Taeluf\PHTML\PHPParser  
Parses .php files into:  
 1. A string with placeholders for the PHP code  
 2. An array of PHP code, identified by their placeholders  
  
  
## Constants  
  
## Properties  
- `protected string $src;` The source HTML + PHP code  
  
- `protected object $pieces;` The parsed pieces of code in the format:  
 [  
     'html' => '<div>A string of code with php placeholders like <b>phpadsfwefhaksldfjaskdlfjaskldfjasldfkphp</b> </div> and trailing text...',  
     'php'  => [  
         'phpadsfwefhaksldfjaskdlfjaskldfjasldfkphp' => '<?="Something echod";?>',   
         'phpid2'=>'<?php // another code block/?>'  
     ]  
 ]  
The array is simply (object) cast  
  
## Methods   
- `public function __construct(string $htmlPHPString)` Create a new PHP parser instance from a string  
  
- `public function pieces(): object` Return the parsed pieces. See doc for protected $pieces  
  
- `protected function separatePHP(): object` Separate the source code into it's pieces. See the protected $pieces docs  
- `protected function randomAlpha($length = 26): string` Generates a random string of characters a-z, all lowercase & returns them  
 this is used for the PHP placeholders  
  
- `static public function getRandomAlpha($length = 26): string` Generates a random string of characters a-z, all lowercase & returns them  
 this is used for the PHP placeholders  
  
- `protected function tokens(): array` Tokenize the src code into a slightly better format than token_get_all  
  
  
