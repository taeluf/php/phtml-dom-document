# File code/Compiler.php  
  
# class Taeluf\PHTML\Compiler  
  
  
  
## Constants  
  
## Properties  
- `protected $code = [];` An array of code to output.  
Likely contains placeholder which will be replaced.   
May contain objects which implement __toString  
- `protected $src;` The content of a PHP file for compilation  
- `protected $placeholder = [];` An array of placeholder code with codeId => [prependedCode, code, appendedCode]... there can be any number of entries for each codeId  
Code may be string or an object which implements __toString  
codeIds are either sha sums (alpha-numeric, i think) or randmoized alpha  
- `protected $htmlSource;` The parsed source code, with the PHP code replaced by placeholders  
  
## Methods   
- `public function __construct()`   
- `public function cleanSource($srcCode): string` Replaces inline PHP code with placeholder, indexes the placeholder, and returns the modified code  
  
- `public function placeholderFor($phpCodeWithOpenCloseTags): string` 1. Generates an id  
2. indexes the passed-in-code with that id  
3. Returns the id.  
  
- `public function appendCode($code)` Appends code to the output-to-be  
  
- `public function prependCode($code)` Prepends code to the output-to-be  
  
- `public function placeholderPrepend($placeholder,$code)` Prepend code immediately prior to the given placeholder  
  
- `public function placeholderAppend($placeholder,$code)` Append code immediately after the given placeholder  
  
- `public function output(): string` Compile the code into a string & return it.   
output() can be called several times as it does NOT affect the state of the compiler.  
  
- `public function writeTo(string $file, $chmodTo=null): bool` Writes the compiled output to the given file  
  
- `public function fileForCode($compileDir,$code): string` Get an absolute file path which can be included to execute the given code  
  
1. $codeId = sha1($code)  
2. file_put_contents("$compileDir/$codeId.php", $code)  
3. return the path of the new file  
  
- Will create the directory (non-recursive) if not exists  
  
- `protected function freshId($length = 26): string` Generate a random string of lowercase letters  
  
- `public function codeForId(string $codeId,bool $asArray=false)` Get the code for the given code id.  
  
Placeholder code is stored as an array to enable the placeholderPrepend|Append functions, so I make it available as an array if you want.  
  
  
  
