# pHtml Status


## Next
- Add a TextNode class, then add create a base class that both TextNode & my DOMNode extend from with some common functionality

## Latest accomplishments
- `__get($propName)` now calls `getPropName()` if the method exists on `Phtml\Node` & add `getInnerText()`.
- insertCodeBefore & insertCodeAfter
- `$node->innerHtml` is case insensitive
- Php code can be added to a tag declartion, without causing attribute mumbo jumbo problems.


## Ideas
- I DONT like this idea very much. Inside `<head>` use a `<randid>` node instead of `phprandidphp`... As one way to allow php inside the `<head>`.
    1. Do current "replace all php with randids" thing.
    2. Do current `<head>` for `<tlf-head>` replacement
    3. In DOMDocument land, find the phprandidphp TEXT Node & replace it with an actual node (since `<head>` doesn't support text nodes)
    4. convert `<tlf-head>` back to `<head>`
- I don't think its worth the effort, since my current approach... works... Stop using a `<root>` node. Use an xml type OR... smartly "know" whether the source html was full proper html or not. if not, then just put it to the `<body>` and get from the `<body>`...


## Documentation needed
### DomNode custom Properties
- children
- hideOwnTag
- `$node->attributeName` (uses magic `__get`)
- @todo `phtml` as alias for `ownerDocument`
- ...

### DomNode custom methods
- addHiddenInput
-`__toString`

### DomDocument custom methods
- adPhpToTag($node, $php)
- xpath
- phpPlaceholder
- setDynamicAttribute($code)
- insertCodeBefore($node, $code);
- insertCodeAfter($node, $code);

### DomNode builtin properties
- ownerDocument
- tagName

### DomNode builtin methods
- hasAttribute, getAttribute, setAttribute

### DomDocument builtin methods

## Stuff
- My test coverage is not very good.
- all `foreach ($node->childNodes)` should be replaced with `foreach($node->children)` (probably)
- Extensible property-getter system, so I don't have to keep using `if/else` statements inside `__get`.
- `Compiler` is not used & should be removed. (or is it `PHPParser` to remove?)
- Branch naming (make a `v0.1`)
- change to `pHtml` from `PHTML` class name
- when `innerHtml/innerText` is used improperly, throw exception & inform correct property capitalizatin to use.
